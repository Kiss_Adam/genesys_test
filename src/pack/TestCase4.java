package pack;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.openqa.selenium.JavascriptExecutor;

public class TestCase4 {

	@Test
	public static void TestCase_4() throws InterruptedException {
		
		// Open new Chrome window and maximize
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		// Open page
		driver.get("http://automationpractice.com/index.php");
		
		// Get page parameters
		int pageHeight = driver.findElement(By.id("page")).getSize().height;
		int footerHeight = driver.findElement(By.id("footer")).getSize().height;
		int visibleAreaHeight = ((Number) js.executeScript("return window.innerHeight")).intValue();
		int totalScrollDistance = pageHeight - footerHeight - visibleAreaHeight;
		
		// Scroll until footer is visible
		while (totalScrollDistance >= 0) {
			js.executeScript("window.scrollBy(0, 100)");
			totalScrollDistance -= 100;
			Thread.sleep(250);
		}
		
		driver.quit();
	}

}
