package pack;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;
import okhttp3.*;

public class TestCase5 {
	private static final Logger LOGGER = Logger.getLogger(TestCase5.class);

	@Test
	public static void TestCase_5() throws InterruptedException, IOException, ParseException {
		
		// Define new arrayLists for names and emails
		ArrayList<String> userNames = new ArrayList<String>();
		ArrayList<String> userEmails = new ArrayList<String>();
		
		// Go through all 10 users and get JSON via response independently
		for (int i = 1; i <= 10; i++) {
			String url = new String("https://jsonplaceholder.typicode.com/users/" + i);
			OkHttpClient client = new OkHttpClient();
			
			// Send request to url
			Request request = new Request.Builder()
			    .url(url)
			    .build();
			
			// Get and store response
			Response response = client.newCall(request).execute();
			String jsonData = response.body().string();
			
			// Verify if response is formatted as JSON
			try {
		        new JSONObject(jsonData);
		    } catch (JSONException ex1) {
		        try {
		            new JSONArray(jsonData);
		        } catch (JSONException ex2) {
					LOGGER.error("TC5 - ERROR: Data received, invalid JSON format!");
		        }
		    }
			
			// Parse response to JSON Object
			JSONObject jsonObject = new JSONObject(jsonData);
				
			//Get name and email fields from JSON, store in arrayList and log
			userNames.add(jsonObject.getString("name"));
			userEmails.add(jsonObject.getString("email"));
			
			LOGGER.warn("TC5 - STATUS: " 
						+ jsonObject.getString("name") 
						+ " | " 
						+ jsonObject.getString("email"));
		}
		
		// Verify if 1st email contains @
		boolean containsAt = userEmails.get(0).contains("@");
		if (containsAt) {
			LOGGER.warn("TC5 - PASSED: First email contains @");
		} else {
			LOGGER.error("TC5 - ERROR: First email invalid!");
		}
	}
}