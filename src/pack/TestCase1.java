package pack;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;


public class TestCase1 {
	private static final Logger LOGGER = Logger.getLogger(TestCase1.class);
	
	@Test
	public static void TestCase_1() throws InterruptedException{

		// Open new Chrome window and maximize
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		// Open page and click Sign In
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]")).click();
		Thread.sleep(2000);
		
		// Write email to Sign Up field and click Submit
		driver.findElement(By.xpath("//*[@id=\"email_create\"]")).sendKeys("TC1_random011@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"SubmitCreate\"]")).click();
		Thread.sleep(5000);
		Boolean error = false;
		
		// Check if email has already been registered
		try {
			error = driver.findElement(By.xpath("//*[@id=\"create_account_error\"]")).isEnabled();
		} catch (NoSuchElementException e){
			LOGGER.warn("TC1 - STATUS: Email has not been registered earlier");
		}
		if (error) {
			LOGGER.error("TC1 - FAILED: Email already registered!");
			driver.quit();
		}
		
		// Fill out reqired fields in form
		driver.findElement(By.xpath("//*[@id=\"id_gender1\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"customer_firstname\"]")).sendKeys("Adam");
		driver.findElement(By.xpath("//*[@id=\"customer_lastname\"]")).sendKeys("Kiss");
		driver.findElement(By.xpath("//*[@id=\"passwd\"]")).sendKeys("qwert12345");
		
		Select drpDays = new Select(driver.findElement(By.xpath("//*[@id=\"days\"]")));
		drpDays.selectByValue("14");
		Select drpMonths = new Select(driver.findElement(By.xpath("//*[@id=\"months\"]")));
		drpMonths.selectByValue("5");
		Select drpYears = new Select(driver.findElement(By.xpath("//*[@id=\"years\"]")));
		
		drpYears.selectByValue("1995");
		driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Adam");
		driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("Kiss");
		driver.findElement(By.xpath("//*[@id=\"address1\"]")).sendKeys("asd str 123.");
		driver.findElement(By.xpath("//*[@id=\"city\"]")).sendKeys("Budapest");
		
		Select drpState = new Select(driver.findElement(By.xpath("//*[@id=\"id_state\"]")));
		drpState.selectByVisibleText("Georgia");
		
		driver.findElement(By.xpath("//*[@id=\"postcode\"]")).sendKeys("01234");
		
		Select drpCountry = new Select(driver.findElement(By.xpath("//*[@id=\"id_country\"]")));
		drpCountry.selectByVisibleText("United States");
		
		driver.findElement(By.xpath("//*[@id=\"phone_mobile\"]")).sendKeys("+36123456789");
		driver.findElement(By.xpath("//*[@id=\"alias\"]")).sendKeys("ASDqwert123");
		
		// Click register then verify My account page
		driver.findElement(By.xpath("//*[@id=\"submitAccount\"]")).click();
		Thread.sleep(2000);
		
		Boolean verifyTitle = driver.getTitle().equals("My account - My Store");
		
		if (verifyTitle) {
			LOGGER.warn("TC1 - PASSED: New account has been registered successfully!");
		} else {
			LOGGER.error("TC1 - FAILED: Registration failed!");
		}
		
		driver.quit();
	}
}
