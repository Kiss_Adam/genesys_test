package pack;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

public class TestCase3 {
	private static final Logger LOGGER = Logger.getLogger(TestCase3.class);

	@Test
	public static void TestCase_3() throws InterruptedException {
		
		// Open new Chrome window and maximize
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		// Open page and get handle and iFrame parameters
		driver.get("http://demo.guru99.com/test/guru99home/");
		
		String mainTab = driver.getWindowHandle();
		
		driver.switchTo().frame("a077aa5e");
		driver.findElement(By.xpath("/html/body/a/img")).click();
		Thread.sleep(5000);
		
		// Get new tab parameters and check if open
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		newTab.remove(mainTab);
		
		driver.switchTo().window(newTab.get(0));
		
		Boolean verifyTitle = driver.getTitle().equals("Selenium Live Project: FREE Real Time Project for Practice");
		
		if (verifyTitle) {
			LOGGER.warn("TC3 - STATUS: New Tab opened successfully!");
		} else {
			LOGGER.error( "TC3 - FAILED: Tab did not open!");
		}
		
		// Close new tab and switch to main tab
		driver.close();
		
	    driver.switchTo().window(mainTab);
		
	    // Hover over Testing menu item and click on Selenium on the visible list
	    Actions action = new Actions(driver);
	    WebElement testingMenu = driver.findElement(By.xpath("//*[@id=\"rt-header\"]/div/div[2]/div/ul/li[2]/a"));
	    action.moveToElement(testingMenu).perform();
	    action.moveToElement(driver.findElement(By.xpath("//*[@id=\"rt-header\"]/div/div[2]/div/ul/li[2]/div/div/ul/li[3]/a"))).click().perform();
	    
	    // Find if Submit is displayed
	    Boolean submitDisplayed = driver.findElement(By.xpath("//*[@id=\"af-body-1742405067\"]/div[2]/input")).isDisplayed();
	
	    if (submitDisplayed) {
			LOGGER.warn("TC3 - PASSED: Submit button displayed!");
	    } else {
			LOGGER.error("TC3 - FAILED: Submit button missing!");
	    }
	    
	    driver.quit();
	}

}
