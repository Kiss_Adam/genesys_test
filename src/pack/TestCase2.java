package pack;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

public class TestCase2 {
	private static final Logger LOGGER = Logger.getLogger(TestCase2.class);
	
	@Test
	public static void TestCase_2() throws InterruptedException {
		
		// Open new Chrome window and maximize
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		// Open page and click Sign In
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]")).click();
		Thread.sleep(2000);
		
		// Write email to Sign Up field and click Submit
		driver.findElement(By.xpath("//*[@id=\"email_create\"]")).sendKeys("TC2_random002@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"SubmitCreate\"]")).click();
		Thread.sleep(5000);
		Boolean emailError = false;
		
		// Check if email has already been registered
		try {
			emailError = driver.findElement(By.xpath("//*[@id=\"create_account_error\"]")).isEnabled();
		} catch (NoSuchElementException e){
			LOGGER.warn("TC2 - STATUS: No error message");
		} finally {
			if (emailError) {
				LOGGER.error("TC2 - FAILED: Email already registered!");
				driver.quit();
			}
		}
		
		// Click on Submit without filling required fields in form
		driver.findElement(By.xpath("//*[@id=\"submitAccount\"]")).click();
		Thread.sleep(5000);
		
		// Check if error message appeared to fill required fields
		Boolean errorDisplayed = false;
		try {
			errorDisplayed = driver.findElement(By.xpath("//*[@class='alert alert-danger']")).isDisplayed();
		} catch (NoSuchElementException e){
			LOGGER.error("TC2 - FAILED: No error message displayed!");
		} finally {
			if (errorDisplayed) {
				LOGGER.warn("TC2 - PASSED: Error popup displayed with missing required fields");
			}
		}
		
		driver.quit();
	}

}
